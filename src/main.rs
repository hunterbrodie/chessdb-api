use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};
use actix_cors::Cors;
use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};
use serde::{Serialize, Deserialize};
use serde_json::json;
use rusqlite::{params, Connection, Result};

#[derive(Serialize)]
struct Player
{
    name: String,
    rating: i64
}

#[derive(Deserialize)]
struct GameInput
{
    white: String,
    black: String,
    result: f64,
}

#[derive(Serialize)]
struct GameOutput
{
    id: i64,
    white: String,
    white_rating: i64,
    black: String,
    black_rating: i64,
    result: f64,
    date: String
}

#[actix_web::main]
async fn main() -> std::io::Result<()>
{
    create_db().unwrap();
    println!("Starting Server");
    
    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file("key.pem", SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file("cert.pem").unwrap();
    
    HttpServer::new(||
        {
            let cors = Cors::default()
            .allow_any_origin()
            .allowed_methods(vec!["GET", "POST"])
            .allow_any_header()
            .max_age(3600);

            App::new()
                .wrap(cors)
                .service(add_game_api)
                .service(players)
                .service(games)
                .service(players_games)
        })
        .bind_openssl("127.0.0.1:8443", builder)?
        .run()
        .await
}

fn create_db() -> Result<()>
{
    //Connect and create tables if needed
    let db = Connection::open("chess.db")?;
    db.execute(
        "CREATE TABLE IF NOT EXISTS players (
            name TEXT NOT NULL UNIQUE,
            rating INTEGER NOT NULL
        )", params![]
    )?;
    db.execute(
        "CREATE TABLE IF NOT EXISTS games (
            id INTEGER PRIMARY KEY,
            white TEXT NOT NULL,
            white_rating INTEGER NOT NULL,
            black TEXT NOT NULL,
            black_rating INTEGER NOT NULL,
            result REAL NOT NULL,
            date DATE DEFAULT CURRENT_DATE
        )",
        params![]
    )?;

    Ok(())
}

#[get("/players")]
async fn players() -> impl Responder
{
    let json = json!(get_players().unwrap());
    HttpResponse::Ok().body(json)
}

fn get_players() -> Result<Vec<Player>>
{
    let db = Connection::open("chess.db")?;
    let mut stmt = db.prepare("SELECT name, rating FROM players")?;
    let players_iter = stmt.query_map(params![], |row| {
        Ok(Player {
            name: row.get(0)?,
            rating: row.get(1)?,
        })
    })?;

    let mut player_vec: Vec<Player> = Vec::new();

    for player in players_iter
    {
        player_vec.push(player.unwrap());
    }

    player_vec.sort_by_key(|k| k.rating);
    player_vec.reverse();

    Ok(player_vec)
}

#[get("/games")]
async fn games() -> impl Responder
{
    let json = json!(get_games().unwrap());
    HttpResponse::Ok().body(json)
}

fn get_games() -> Result<Vec<GameOutput>>
{
    let db = Connection::open("chess.db")?;
    let mut stmt = db.prepare("SELECT id, white, white_rating, black, black_rating, result, date FROM games")?;
    let games_iter = stmt.query_map(params![], |row| {
        Ok(GameOutput {
            id: row.get(0)?,
            white: row.get(1)?,
            white_rating: row.get(2)?,
            black: row.get(3)?,
            black_rating: row.get(4)?,
            result: row.get(5)?,
            date: row.get(6)?
        })
    })?;

    let mut game_vec: Vec<GameOutput> = Vec::new();

    for game in games_iter
    {
        game_vec.push(game.unwrap());
    }

    Ok(game_vec)
}

#[get("/{player}/games")]
async fn players_games(web::Path(name): web::Path<String>) -> impl Responder
{
    let json = json!(get_players_games(name).unwrap());
    HttpResponse::Ok().body(json)
}

fn get_players_games(name: String) -> Result<Vec<GameOutput>>
{
    let db = Connection::open("chess.db")?;
    let mut stmt = db.prepare(format!("SELECT id, white, white_rating, black, black_rating, result, date FROM games WHERE white = '{}' OR black = '{}'", name, name).as_str())?;
    let games_iter = stmt.query_map(params![], |row| {
        Ok(GameOutput {
            id: row.get(0)?,
            white: row.get(1)?,
            white_rating: row.get(2)?,
            black: row.get(3)?,
            black_rating: row.get(4)?,
            result: row.get(5)?,
            date: row.get(6)?
        })
    })?;

    let mut game_vec: Vec<GameOutput> = Vec::new();

    for game in games_iter
    {
        game_vec.push(game.unwrap());
    }

    Ok(game_vec)  
}

#[post("/add-game")]
async fn add_game_api(req_body: String) -> impl Responder
{
    let game = serde_json::from_str(&req_body).unwrap();
    add_game_db(game).unwrap();
    HttpResponse::Ok().body(req_body)
}

fn add_game_db(game: GameInput) -> Result<()>
{
    println!("white: {}, black: {}, result: {}", &game.white, &game.black, &game.result);

    //Connect
    let db = Connection::open("chess.db")?;

    //Insert players
    db.execute(
        "INSERT OR IGNORE INTO players(name, rating) VALUES(?1, 1000)",
        params![game.white]
    )?;
    db.execute(
        "INSERT OR IGNORE INTO players(name, rating) VALUES(?1, 1000)",
        params![game.black]
    )?;

    //Get ratings
    let white_rating: i64 = db.query_row("SELECT * FROM players WHERE name = ?1", params![game.white], |r| r.get(1))?;

    let black_rating: i64 = db.query_row("SELECT * FROM players WHERE name = ?1", params![game.black], |r| r.get(1))?;

    //Insert Game
    db.execute(
        "INSERT INTO games (white, white_rating, black, black_rating, result) VALUES (?1, ?2, ?3, ?4, ?5)",
        params![game.white, white_rating, game.black, black_rating, game.result]
    )?;

    //Calc ELO and update value
    let mut qa: f64 = 10_f64;
    qa = qa.powf((white_rating as f64) / 400_f64);
    let mut qb: f64 = 10_f64;
    qb = qb.powf((black_rating as f64) / 400_f64);
    let ea = qa / (qa + qb);
    let eb = qb / (qa + qb);

    let white_rating_new = (white_rating as f64 + 32_f64 * (game.result - ea)) as i64;
    let black_rating_new = (black_rating as f64 + 32_f64 * (1_f64 - game.result - eb)) as i64;

    //Update tables
    db.execute(
        "UPDATE players
        SET rating = ?2
        WHERE name = ?1",
        params![game.white, white_rating_new]
    )?;
    db.execute(
        "UPDATE players
        SET rating = ?2
        WHERE name = ?1",
        params![game.black, black_rating_new]
    )?;

    Ok(())
}